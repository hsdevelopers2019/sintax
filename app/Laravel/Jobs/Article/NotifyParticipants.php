<?php

namespace App\Laravel\Jobs\Article;

use App\Laravel\Models\User;
use Illuminate\Bus\Queueable;
use App\Laravel\Models\ArticleComment;
use App\Laravel\Models\ArticleReaction;
use App\Laravel\Models\Article;

use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Laravel\Notifications\Article\NewCommentNotification;
use App\Laravel\Notifications\Article\NewReactionNotification;


use Carbon;


class NotifyParticipants implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $user;
    protected $comment,$article,$type;
    /**
     * Create a new job instance.
     *
     * @param  User  $user
     * @param  Chat  $comment
     * @return void
     */
    public function __construct(User $user, Article $article,$type = "NEW_COMMENT")
    {
        $this->user = $user;
        $this->article = $article;
        $this->type = $type;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        switch($this->type){
            case "NEW_COMMENT":
                $date = (new Carbon)->subDay();
                $users_commented = ArticleComment::where('article_id',$this->article->id)
                                        ->where('user_id','<>',$this->user->id)
                                        ->where('created_at',">=",$date)->pluck('user_id')->toArray();
                $unique = array_unique($users_commented);

                if(count($unique) > 1){
                    $user_ids = implode(",", $unique);

                    $content = "{$this->user->name} and ".count($unique)." ".str_plural('other', count($unique))." also commented on the article.";
                    $users = User::whereRaw("id IN ({$user_ids})")->get();

                    foreach($users as $index => $user){
                        if($user->id != $this->article->user_id){
                            $user->notify(new NewCommentNotification($this->article,$content));
                        }
                    }
                }

                if(count($unique) == 1){
                    $other_user = User::find($unique[0]);

                    $content = "{$this->user->name} and {$other_user->name} commented on the article.";

                    $user_ids = implode(",", $unique);
                    $users = User::whereRaw("id IN ({$user_ids})")->get();

                    foreach($users as $index => $user){
                        if($user->id == $other_user->id){
                            $content = "{$this->user->name} and You commented on the article.";
                        }
                        $user->notify(new NewCommentNotification($this->article,$content));
                    }
                }
            break;
            case "NEW_REACTION":
                $date = (new Carbon)->subDay();
                $users_liked = ArticleReaction::where('article_id',$this->article->id)
                                        ->where('is_active','yes')
                                        ->where('user_id','<>',$this->user->id)
                                        ->where('updated_at',">=",$date)->pluck('user_id')->toArray();
                $unique = array_unique($users_liked);

                if(count($unique) > 1){
                    $user_ids = implode(",", $unique);

                    $content = "{$this->user->name} and ".count($unique)." ".str_plural('other', count($unique))." also reacted on the article.";
                    $users = User::whereRaw("id IN ({$user_ids})")->get();

                    foreach($users as $index => $user){
                        if($user->id != $this->article->user_id){
                            $user->notify(new NewReactionNotification($this->article,$content));
                        }
                    }
                }

                if(count($unique) == 1){
                    $other_user = User::find($unique[0]);

                    $content = "{$this->user->name} and {$other_user->name} reacted on the article.";

                    $user_ids = implode(",", $unique);
                    $users = User::whereRaw("id IN ({$user_ids})")->get();

                    foreach($users as $index => $user){
                        if($user->id == $other_user->id){
                            $content = "{$this->user->name} and You reacted on the article.";
                        }
                        $user->notify(new NewReactionNotification($this->article,$content));
                    }
                }
            break;
        }

    }
}