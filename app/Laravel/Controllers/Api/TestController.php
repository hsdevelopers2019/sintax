<?php

namespace App\Laravel\Controllers\Api;

use App\Laravel\Models\WishlistTransaction;
use App\Laravel\Models\WishlistViewer;
use App\Laravel\Notifications\PusherNotification;
use App\Laravel\Transformers\GeneralRequestTransformer;
use App\Laravel\Transformers\MasterTransformer;
use App\Laravel\Transformers\TransformerManager;
use App\Laravel\Transformers\WishlistTransactionTransformer;
use App\Laravel\Transformers\WishlistViewerTransformer;
use App\Laravel\Transformers\QrTransformer;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use App\laravel\models\Qr;

use Str, Carbon, DB, Helper;

class TestController extends Controller
{

	protected $data = array();

    public function __construct() {
        $this->response = array(
            "msg" => "Bad Request.",
            "status" => FALSE,
            'status_code' => "BAD_REQUEST"
            );
        $this->response_code = 400;
        $this->transformer = new TransformerManager;
    }


    public function qr_id(Request $request, $format = '')
    {

        // $brand = $request->get('brand');
        $id  = $request->get('id');
        $brand  = $request->get('brand');

        if(Qr::where('brand','=',$brand)->count() > 0)
        {
            $this->response['msg'] = "QR LIST";
            $this->response['status_code'] = "LIST_OF_QR";
            $this->response['status'] = TRUE;

            // $qr = Qr::find($id);
            $qr = Qr::where('id','=',$id)->get();

        

        $this->response['data'] = $this->transformer->transform($qr, new QrTransformer, 'collection');

        $this->response_code = 200;

        return response()->json($this->response,$this->response_code);
        }
        else
        {
            $this->response['data'] = array(['message' =>'no data found']);
            
            $this->response['msg'] = "QR LIST";
            $this->response['status_code'] = "LIST_OF_QR";
            $this->response['status'] = TRUE;
            return response()->json($this->response,$this->response_code);

        }



        
    }

 
}
