<?php

namespace App\Laravel\Controllers\Api;

use App\Laravel\Models\User;
use App\Laravel\Models\EmailVerification;

use Illuminate\Http\Request;
use App\Laravel\Models\UserDevice;
use ImageUploader, Helper, JWTAuth, Str, Auth, Event, AuditRequest,Carbon;
use App\Laravel\Requests\Api\AvatarRequest;
use App\Laravel\Requests\Api\ProfileCurrencyRequest;

use App\Laravel\Requests\Api\ProfileRequest;
use App\Laravel\Requests\Api\PasswordRequest;
use App\Laravel\Requests\Api\ProfileDescriptionRequest;

use App\Laravel\Transformers\UserTransformer;
use App\Laravel\Transformers\NotificationTransformer;

use App\Laravel\Transformers\TransformerManager;
use App\Laravel\Requests\Api\FacebookLoginRequest;
use App\Laravel\Events\AuditTrailActivity;
use App\Laravel\Notifications\SendEmailVerificationToken;


class ProfileController extends Controller
{

	protected $data = array();

    public function __construct() {
        $this->response = array(
            "msg" => "Bad Request.",
            "status" => FALSE,
            'status_code' => "BAD_REQUEST"
            );
        $this->response_code = 400;
        $this->transformer = new TransformerManager;
    }

    public function show(Request $request, $format = '') {

    	$user = $request->user();

        $this->response['msg'] = "Account details.";
        $this->response['status'] = TRUE;
        $this->response['status_code'] = "PROFILE_INFO";
        $this->response['data'] = $this->transformer->transform($user, new UserTransformer, 'item');
        $this->response_code = 200;

        callback:
        switch(Str::lower($format)){
            case 'json' :
                return response()->json($this->response, $this->response_code);
            break;
            case 'xml' :
                return response()->xml($this->response, $this->response_code);
            break;
        }
    }

    public function fb_connect(FacebookLoginRequest $request, $format = '') {

        $user = $request->user();
        $email = $request->get('email') ?: $request->get('fb_id')."@facebook.com";

        $user->fb_id = $request->get('fb_id');
        $user->save();

        $this->response['msg'] = Helper::get_response_message("FACEBOOK_CONNECT_SUCCESS");
        $this->response['status'] = TRUE;
        $this->response['status_code'] = "FACEBOOK_CONNECT_SUCCESS";
        $this->response['data'] = $this->transformer->transform($user, new UserTransformer,'item');
        $this->response_code = 200;

        callback:
        switch(Str::lower($format)){
            case 'json' :
                return response()->json($this->response, $this->response_code);
            break;
            case 'xml' :
                return response()->xml($this->response, $this->response_code);
            break;
        }
    }

    public function update_profile(ProfileRequest $request, $format = '') {
        $ip = AuditRequest::header('X-Forwarded-For');
        if(!$ip) $ip = AuditRequest::getClientIp();

        $user = $request->user();
        // $user->fill($request->except('password','type'));
        // $user->email = Str::lower($request->get('email'));
        $user->username = Str::lower($request->get('username'));
        $user->country_iso = $request->get('country_iso');
        $user->country_code = $request->get('country_code');
        $user->contact_number = str_replace($user->country_code, "", $request->get('contact_number'));
        $user->specialty_id = "0";
        $user->specialties = "0";
        $user->last_activity = Carbon::now();

        // $user->specialty_id = $request->get('specialty_id');
        // $user->specialties = implode(",", array_map("trim",explode(",", $request->get('specialty_id'))));
        $specialties =  explode(",", $request->get('specialty_id'));
        $final_specialties = [];
        foreach($specialties as $index => $specialty){
            if(strlen($specialty) > 0){
                array_push($final_specialties, $specialty);
            }
        }
        $user->specialties = implode(",", $final_specialties);

        if(strlen($user->type) == 0 OR $user->type == "user"){
            $user->type = $request->get('type');

            if($user->type == "mentee" AND $user->is_approved == "pending"){
                $user->is_approved = "yes";
            }
        }


        switch($user->type){
            case 'mentor':
                $user->city = $request->get('city');
                $user->state = $request->get('state');
                $user->address1 = $request->get('address1');
                $user->address2 = $request->get('address2');
            break;
            default:
                $user->city = "";
                $user->state = "";
                $user->address1 = "";
                $user->address2 = "";
        }

        $user->save();

        $user = User::find($user->id);

        $this->response['msg'] = "Account successfully modified. Please refresh your screen when changes didn't take effect.";
        $this->response['status'] = TRUE;
        $this->response['status_code'] = "PROFILE_UPDATED";
        $this->response_code = 200;
        $this->response['data'] = $this->transformer->transform($user, new UserTransformer, 'item');

        $log_data = new AuditTrailActivity(['user_id' => Auth::user()->id,'process' => "PROFILE_UPDATED", 'remarks' => Auth::user()->name." was successfully updated his/her profile.",'ip' => $ip]);
        Event::fire('log-activity', $log_data);

        callback:
        switch(Str::lower($format)){
            case 'json' :
                return response()->json($this->response, $this->response_code);
            break;
            case 'xml' :
                return response()->xml($this->response, $this->response_code);
            break;
        }
    }

    public function update_description(ProfileDescriptionRequest $request, $format = '') {
        $ip = AuditRequest::header('X-Forwarded-For');
        if(!$ip) $ip = AuditRequest::getClientIp();

        $user = $request->user();

        $user->description = $request->get('description');
        $user->save();
        $user = User::find($user->id);
        

        $this->response['msg'] = "Account description successfully modified. Please refresh your screen when changes didn't take effect.";
        $this->response['status'] = TRUE;
        $this->response['status_code'] = "PROFILE_UPDATED";
        $this->response_code = 200;
        $this->response['data'] = $this->transformer->transform($user, new UserTransformer, 'item');

        callback:
        switch(Str::lower($format)){
            case 'json' :
                return response()->json($this->response, $this->response_code);
            break;
            case 'xml' :
                return response()->xml($this->response, $this->response_code);
            break;
        }
    }


    public function update_avatar(AvatarRequest $request, $format = '') {
        $ip = AuditRequest::header('X-Forwarded-For');
        if(!$ip) $ip = AuditRequest::getClientIp();

        $user = $request->user();
        if($request->hasFile('file')) {
            $image = ImageUploader::upload($request->file('file'), "uploads/images/users/{$user->id}");
            $user->path = $image['path'];
            $user->directory = $image['directory'];
            $user->filename = $image['filename'];
            $user->source = $image['source'];
        }

        $user->save();

        $this->response['msg'] = /*Helper::get_response_message("AVATAR_UPDATED")*/ "Avatar updated.";
        $this->response['status'] = TRUE;
        $this->response['status_code'] = "AVATAR_UPDATED";
        $this->response_code = 200;
        $this->response['data'] = $this->transformer->transform($user, new UserTransformer, 'item');

        $log_data = new AuditTrailActivity(['user_id' => Auth::user()->id,'process' => "AVATAR_UPDATED", 'remarks' => Auth::user()->name." was successfully your avatar.",'ip' => $ip]);
        Event::fire('log-activity', $log_data);

        callback:
        switch(Str::lower($format)){
            case 'json' :
                return response()->json($this->response, $this->response_code);
            break;
            case 'xml' :
                return response()->xml($this->response, $this->response_code);
            break;
        }
    }
    
    public function update_password(PasswordRequest $request, $format = '') {
        $ip = AuditRequest::header('X-Forwarded-For');
        if(!$ip) $ip = AuditRequest::getClientIp();
        
    	$user = $request->user();
        $user->password = bcrypt($request->get('password'));
        $user->save();
        
        $this->response['msg'] = "Account password successfully modified.";
        $this->response['status'] = TRUE;
        $this->response['status_code'] = "PASSWORD_UPDATED";
        $this->response_code = 200;
        $this->response['data'] = $this->transformer->transform($user, new UserTransformer, 'item');

        $log_data = new AuditTrailActivity(['user_id' => Auth::user()->id,'process' => "PASSWORD_UPDATED", 'remarks' => Auth::user()->name." was successfully your password.",'ip' => $ip]);
        Event::fire('log-activity', $log_data);

        callback:
        switch(Str::lower($format)){
            case 'json' :
                return response()->json($this->response, $this->response_code);
            break;
            case 'xml' :
                return response()->xml($this->response, $this->response_code);
            break;
        }
    }

    public function update_device(Request $request, $format = '') {

        if($request->has('device_id')){

            $device = UserDevice::where('user_id', $user->id)->where('device_id',  $request->get('device_id'))->first();

            UserDevice::where('device_id', $request->get('device_id'))
                        ->where('user_id','<>',$user->id)
                        ->update(['is_login' => "0"]);
                        
            if(!$device){
                $new_device = new UserDevice;
                $new_device->user_id = $user->id;
                $new_device->reg_id =  $request->get('device_reg_id');
                $new_device->device_id =  $request->get('device_id')?:'';
                $new_device->device_name =  $request->get('device_name');
                $new_device->is_login = 1;
                $new_device->save();
            }else{
                $device->reg_id =  $request->get('device_reg_id')?:$device->reg_id;
                $device->device_name =  $request->get('device_name');
                $device->is_login = 1;
                $device->save();
            }
        }
        
        $this->response['msg'] = "User device updated successfully.";
        $this->response['status'] = TRUE;
        $this->response['status_code'] = "DEVICE_UPDATED";
        $this->response_code = 200;
        $this->response['data'] = $this->transformer->transform($user, new UserTransformer, 'item');

        callback:
        switch(Str::lower($format)){
            case 'json' :
                return response()->json($this->response, $this->response_code);
            break;
            case 'xml' :
                return response()->xml($this->response, $this->response_code);
            break;
        }
    }

    public function resend_verification(Request $request, $format = ''){
        $user = $request->user();

        if($user->is_verified == "yes"){
            $this->response['msg'] = "Unable to proceed request. Account already verified.";
            $this->response['status'] = FALSE;
            $this->response['status_code'] = "ACCOUNT_ALREADY_VERIFIED";
            $this->response_code = 400;
            goto callback;

        }

        $token = Str::upper($this->_generateEmailVerificationToken());

        $user->notify(new SendEmailVerificationToken($token, ['source' => "api", 'name' => $user->username, 'email' => $user->email]) );
        // $user->notify(new SendWelcomeMessage() );
        $this->_saveEmailVerificationToken($user, $token);

        $this->response['msg'] = "Email verification successfully sent.";
        $this->response['status'] = TRUE;
        $this->response['status_code'] = "EMAIL_VERIFICATION";
        $this->response_code = 200;
        $this->response['data'] = $this->transformer->transform($user, new UserTransformer, 'item');

        callback:
        switch(Str::lower($format)){
            case 'json' :
                return response()->json($this->response, $this->response_code);
            break;
            case 'xml' :
                return response()->xml($this->response, $this->response_code);
            break;
        }
    }

    public function notifications(Request $request,$format = ''){
        $user = $request->user();
        $user->last_notification_check = Helper::datetime_db(Carbon::now());
        $user->save();

        $per_page = $request->get('per_page', 10);
        $page = $request->get('page', 1);

        $notifications = $user->notifications()->orderBy('created_at',"DESC")->paginate($per_page);

        $this->response['msg'] = "List of notification.";
        $this->response['status'] = TRUE;
        $this->response['status_code'] = "NOTIFICATION_DATA";
        $this->response['has_morepages'] = $notifications->hasMorePages();
        $this->response['data'] = $this->transformer->transform($notifications, new NotificationTransformer, 'collection');
        $this->response_code = 200;

        callback:
        switch(Str::lower($format)){
            case 'json' :
                return response()->json($this->response, $this->response_code);
            break;
            case 'xml' :
                return response()->xml($this->response, $this->response_code);
            break;
        }
    }

    private function _generateEmailVerificationToken() {
        $key = config('app.key');
        if (Str::startsWith($key, 'base64:')) {
            $key = base64_decode(substr($key, 7));
        }
        // return hash_hmac('sha256', Str::random(40), $key);
        return Str::random(6);
    }

    private function _saveEmailVerificationToken(User $user, $token) {
        EmailVerification::where('email', $user->email)->delete();

        $email_verification = new EmailVerification;
        $email_verification->email = $user->email;
        $email_verification->token = $token;
        $email_verification->created_at = Carbon::now();
        $email_verification->save();
    }
}
