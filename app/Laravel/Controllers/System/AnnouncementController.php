<?php 

namespace App\Laravel\Controllers\System;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\Announcement;

/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\System\AnnouncementRequest;

use App\Laravel\Jobs\Announcement\NotifyUsers;
use App\Laravel\Notifications\Self\Article\ArticleApprovedNotification;
use App\Laravel\Notifications\Self\Article\ArticleForceNotification;

/**
*
* Classes used for this controller
*/
use Helper, Carbon, Session, Str,ImageUploader;

class AnnouncementController extends Controller{

	/**
	*
	* @var Array $data
	*/
	protected $data;

	public function __construct () {
		$this->data = [];
		parent::__construct();
		array_merge($this->data, parent::get_data());
		$this->data['user_notified'] = [ "all" => "Everyone",'mentor' => "Mentors Only","mentee" => "Mentee Only"];
		$this->data['heading'] = "Announcement";
	}

	public function index () {
		$this->data['page_title'] = " :: Announcement - Record Data";
		$this->data['announcements'] = Announcement::orderBy('updated_at',"DESC")->paginate(15);
		return view('system.announcement.index',$this->data);
	}

	public function create () {
		$this->data['page_title'] = " :: Announcement - Add new record";
		return view('system.announcement.create',$this->data);
	}

	public function store (AnnouncementRequest $request) {
		try {
			$new_announcement = new Announcement;
			$user = $request->user();
        	$new_announcement->fill($request->only('title','content','user_notified'));
			if($request->hasFile('file')) {
			    $image = ImageUploader::upload($request->file('file'), "uploads/images/announcements");
			    $new_announcement->path = $image['path'];
			    $new_announcement->directory = $image['directory'];
			    $new_announcement->filename = $image['filename'];
            	$new_announcement->source = $image['source'];

			}
			// $new_announcement->status = $request->get('status');
			$new_announcement->user_id = $request->user()->id;
			if($new_announcement->save()) {
				dispatch(new NotifyUsers($new_announcement->user_notified,$new_announcement));

				session()->flash('notification-status','success');
				session()->flash('notification-msg',"New record has been added.");
				return redirect()->route('system.announcement.index');
			}
			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');

			return redirect()->back();
		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function edit ($id = NULL) {
		$this->data['page_title'] = " :: Announcement - Edit record";
		$announcement = Announcement::find($id);

		if (!$announcement) {
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Record not found.");
			return redirect()->route('system.announcement.index');
		}

		if($id < 0){
			session()->flash('notification-status',"warning");
			session()->flash('notification-msg',"Unable to update special record.");
			return redirect()->route('system.announcement.index');	
		}

		$this->data['announcement'] = $announcement;
		return view('system.announcement.edit',$this->data);
	}

	public function update (AnnouncementRequest $request, $id = NULL) {
		try {
			$announcement = Announcement::find($id);

			if (!$announcement) {
				session()->flash('notification-status',"failed");
				session()->flash('notification-msg',"Record not found.");
				return redirect()->route('system.announcement.index');
			}

			if($id < 0){
				session()->flash('notification-status',"warning");
				session()->flash('notification-msg',"Unable to update special record.");
				return redirect()->route('system.announcement.index');	
			}
			$user = $request->user();
        	$announcement->fill($request->only('title','content','user_notified'));
        	if($request->hasFile('file')) {
        	    $image = ImageUploader::upload($request->file('file'), "uploads/images/announcements");
        	    $announcement->path = $image['path'];
        	    $announcement->directory = $image['directory'];
        	    $announcement->filename = $image['filename'];
            	$announcement->source = $image['source'];
        	}

			// dispatch(new NotifyUsers($owner, $announcement));

			if($announcement->save()) {
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"Record has been modified successfully.");
				return redirect()->route('system.announcement.index');
			}

			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function force_notification($id = NULL){
		$announcement = Announcement::find($id);

		if (!$announcement) {
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Record not found.");
			return redirect()->route('system.announcement.index');
		}
		// $owner->notify(new ArticleForceNotification($announcement));
		dispatch(new NotifyUsers($announcement->user_notified,$announcement));

		session()->flash('notification-status','success');
		session()->flash('notification-msg',"Force Push notification successfully sent.");
		return redirect()->route('system.announcement.index');
	}

	public function destroy ($id = NULL) {
		try {
			$announcement = Announcement::find($id);

			if (!$announcement) {
				session()->flash('notification-status',"failed");
				session()->flash('notification-msg',"Record not found.");
				return redirect()->route('system.announcement.index');
			}

			if($id < 0){
				session()->flash('notification-status',"warning");
				session()->flash('notification-msg',"Unable to remove special record.");
				return redirect()->route('system.announcement.index');	
			}

			if($announcement->delete()) {
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"Record has been deleted.");
				return redirect()->route('system.announcement.index');
			}

			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

}