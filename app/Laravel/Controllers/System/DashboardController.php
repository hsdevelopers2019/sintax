<?php 

namespace App\Laravel\Controllers\System;

/*
*
* Models used for this controller
*/
use App\Laravel\Models\User;
use App\Laravel\Models\Article;
use App\Laravel\Models\Mentorship;




/*
*
* Requests used for validating inputs
*/


/*
*
* Classes used for this controller
*/
use Helper, Carbon, Session, Str, DB;

class DashboardController extends Controller{

	/*
	*
	* @var Array $data
	*/
	protected $data;

	public function __construct () {
		$this->data = [];
		parent::__construct();
		array_merge($this->data, parent::get_data());
	}

	public function index(){
		$this->data['page_title'] = " :: Dashboard & Statistics";
		$mentors = User::where('type','mentor')->count();
		$pending_mentors = User::where('type','mentor')->count();
		$mentees = User::where('type','mentee')->count();
		$total = $mentors + $mentees;
		if($total <= 0) $total = 1;
		$date_today = Carbon::now()->format("Y-m-d");

		$this->data['mentors_percentage'] = (int)round(($mentors/$total)*100);
		$this->data['mentees_percentage'] = (int)round(($mentees/$total)*100);
		$this->data['mentors'] = $mentors;
		$this->data['active_mentees'] = User::where('type','user')->count();
		$this->data['active_mentors'] = User::where('type','user')->count();
		$this->data['pending_mentors'] = $pending_mentors;
		$this->data['mentees'] = $mentees;
		$this->data['new_users'] = User::whereRaw("DATE(created_at) = '{$date_today}'")
										->whereIn('type',['mentee','mentor'])->count();
		$this->data['articles'] = 0;
		$this->data['pending_articles'] = 0;	

		$this->data['daily_mentors'] = User::where('type','mentor')->whereRaw("DATE(created_at) = '{$date_today}'")->count();
		$this->data['daily_mentees'] = User::where('type','mentee')->whereRaw("DATE(created_at) = '{$date_today}'")->count(); 				

		// date:"2018-07-09",mentor:2666,mentee:2647

		$area_chart = [];
		$mentorship_chart = [];

		$hourly_registrants = [];

		foreach(range(0,Carbon::now()->format("H")) as $index => $value){
			$hourly_registrant_mentee = User::whereRaw("DATE(created_at) = '".$date_today."'")
											->whereRaw("HOUR(created_at) = '{$value}'")
											->whereIn('type',['mentee'])->count();
			$hourly_registrant_mentor = User::whereRaw("DATE(created_at) = '".$date_today."'")
											->whereRaw("HOUR(created_at) = '{$value}'")
											->whereIn('type',['mentor'])->count();
			$hour = str_pad($value, 2,"0",STR_PAD_LEFT);
			array_push($hourly_registrants, ['date' => Carbon::parse($date_today." {$hour}:00:00")->format("h:i A"),'mentor' => $hourly_registrant_mentor,'mentee' => $hourly_registrant_mentee]);
		}


		$start_date = Carbon::now()->subDays(6);
		// foreach(range(1,6) as $index => $value){
		// 	$mentee_registrant = User::whereRaw("DATE(created_at) = '".$start_date->addDay()->format("Y-m-d")."'")
		// 						->whereIn('type',['user'])->count();
		// 	$mentor_registrant = User::whereRaw("DATE(created_at) = '".$start_date->format("Y-m-d")."'")
		// 						->whereIn('type',['mentor'])->count();
		// 	$mentorships = Mentorship::whereRaw("DATE(created_at) = '".$start_date->format("Y-m-d")."'")->count();
		// 	array_push($area_chart, ['date' => $start_date->format("Y-m-d"),'mentor' => $mentor_registrant,'mentee' => $mentee_registrant]);
		// 	array_push($mentorship_chart, ['date' => $start_date->format("Y-m-d"),'mentorship' => $mentorships ]);
		// }

		$this->data['area_chart'] = json_encode($area_chart);
		$this->data['hourly_registrants'] = json_encode($hourly_registrants);
		$this->data['mentorship_chart'] = json_encode($mentorship_chart);
		return view('system.index',$this->data);
	}
}