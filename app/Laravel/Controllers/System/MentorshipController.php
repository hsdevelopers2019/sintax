<?php 

namespace App\Laravel\Controllers\System;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\Views\Mentorship;
use App\Laravel\Models\MentorshipConversation;

/**
*
* Requests used for validating inputs
*/

/**
*
* Classes used for this controller
*/
use Helper, Carbon, Session, Str,Input;

class MentorshipController extends Controller{

	/**
	*
	* @var Array $data
	*/
	protected $data;

	public function __construct () {
		$this->data = [];
		parent::__construct();
		array_merge($this->data, parent::get_data());
		$this->data['types'] = ['' => "Choose Location Type", 'metro_manila' => "Metro Manila","provincial" => "Provincial"];
		$this->data['heading'] = "Mentorships";
	}

	public function index () {
		$this->data['page_title'] = " :: Mentorships - Record Data";
		$this->data['chats'] = Mentorship::orderBy('latest_message_created_at',"DESC")->paginate(15);
		return view('system.mentorship.index',$this->data);
	}

	

	public function show ($id = NULL) {
		$chat = Mentorship::find($id);
		$per_page = Input::get('per_page',10);
		if (!$chat) {
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Record not found.");
			return redirect()->route('system.mentorship.index');
		}

		$this->data['page_title'] = " :: Mentorships - {$chat->code}";

		$this->data['chat'] = $chat;
		$this->data['conversation'] = MentorshipConversation::where('mentorship_id',$chat->id)->orderBy('created_at',"DESC")->paginate($per_page);
		return view('system.mentorship.show',$this->data);
	}

}