<?php 

namespace App\Laravel\Controllers\System;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\User;
use App\Laravel\Models\Specialty;


/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\System\AppUserRequest;
use App\Laravel\Requests\System\ImportAccountRequest;

/**
*
* Classes used for this controller
*/
use Helper, Carbon, Session, Str,Input,Excel;

class AppUserController extends Controller{

	/**
	*
	* @var Array $data
	*/
	protected $data;
	protected $import = false;

	public function __construct () {
		$this->data = [];
		parent::__construct();
		array_merge($this->data, parent::get_data());
		$this->data['user_types'] = ['' => "Choose Account Type",'user' => "User"];
		$this->data['statuses'] = [ "pending" => "Pending for Approval",'yes' => "Approve account","no" => "Disapprove"];
		
		$this->data['heading'] = "App User";
	}

	public function index () {
		$this->data['page_title'] = " :: App User - Record Data";
		$this->data['keyword'] = Input::get('keyword',false);
		$this->data['from'] = Input::get('from',false);
		$this->data['to'] = Input::get('to',false);
		$this->data['type'] = "";
		$this->data['users'] = User::types(['mentee','mentor'])
								->keyword($this->data['keyword'])
								->registrationDate($this->data['from'],$this->data['to'])
								->orderBy('created_at',"DESC")->paginate(15);
		return view('system.app-user.index',$this->data);
	}

	public function mentors () {
		$this->data['page_title'] = " :: App User - Mentors";
		$this->data['keyword'] = Input::get('keyword',false);
		$this->data['from'] = Input::get('from',false);
		$this->data['to'] = Input::get('to',false);
		$this->data['type'] = "mentor";

		$this->data['users'] = User::types(['mentor'])
								->keyword($this->data['keyword'])
								->registrationDate($this->data['from'],$this->data['to'])
								->orderBy('created_at',"DESC")->paginate(15);
		return view('system.app-user.index',$this->data);
	}

	public function mentees () {
		$this->data['page_title'] = " :: App User - Mentee";
		$this->data['keyword'] = Input::get('keyword',false);
		$this->data['from'] = Input::get('from',false);
		$this->data['to'] = Input::get('to',false);
		$this->data['type'] = "mentee";
		$this->data['users'] = User::types(['mentee'])
								->keyword($this->data['keyword'])
								->registrationDate($this->data['from'],$this->data['to'])
								->orderBy('created_at',"DESC")->paginate(15);
		return view('system.app-user.index',$this->data);
	}

	public function export () {
		$type = Input::get('type',FALSE);
		$this->data['keyword'] = Input::get('keyword',false);
		$this->data['from'] = Input::get('from',false);
		$this->data['to'] = Input::get('to',false);
		// $this->data['status'] = "delivered";
		$users = User::where(function($query) use($type){
									if($type AND strlen($type) > 0){
										return $query->where('type',$type);
									}else{
										return $query->where('type',['mentee','mentor']);
									}
								})
								->keyword($this->data['keyword'])
								->registrationDate($this->data['from'],$this->data['to'])
								->orderBy('created_at',"DESC")
								->get();
		// return view('system.waybill.customer',$this->data);

									// dd($this->data['waybills']);
		Excel::create("Exported Data - ".Carbon::now()->format("m-d-Y h:i:s A"), function($excel) use ($users) {
		    $excel->setTitle("Exported Data".Carbon::now()->format("m-d-Y h:i:s A"))
		        ->setCreator("Richard Kennedy Domingo")
		        ->setCompany('Mentor Me App');
		   $excel->sheet("SUMMARY",function($sheet) use ($users) {
	   			$sheet->setFontFamily('Calibri Light');

	   			$sheet->setColumnFormat(array(
	   			    'A' => '0000'
	   			));

	   			$sheet->row(1,['NAME','EMAIL','CONTACT NUMBER','EXPERTISE','ACCOUNT TYPE','LAST LOGIN','DATE REGISTERED','ACCOUNT STATUS','VERIFIED EMAIL']);
	   			$i = 2;
	   			foreach($users as $index => $user){
	   				$specialties = strlen($user->specialties) != "0" ? $user->specialties : "0";
	   				$user_specialties = Specialty::whereRaw("id IN ($specialties)")->pluck("name")->toArray();
	   				$display_specialties = count($user_specialties) > 0 ? implode(", ", $user_specialties) :"n/a";
	   				switch($user->is_approved){
	   					case 'yes' : $account_status = "Approved";break;
	   					case 'no' : $account_status = "Declined";break;
	   					default: $account_status = "Pending for Approval";
	   				}
	   				$verified_status = "Pending Verification";
	   				if($user->is_verified == "yes") $verified_status = "Vefiried";
   					$sheet->row($i++,[$user->name,$user->email,($user->country_code.$user->contact_number),$display_specialties,$user->type,$user->last_activity? Helper::date_format($user->last_activity,"m/d/Y h:i A") :"n/a",Helper::date_format($user->created_at,"m/d/Y h:i A"),$account_status,$verified_status]);
	   			}
		   });
		})->export('xls');
	}

	public function create () {
		$this->data['page_title'] = " :: App User - Add new account";
		return view('system.app-user.create',$this->data);
	}

	public function store (AppUserRequest $request) {
		try {
			$new_user = new User;
			$new_user->fill($request->only('name', 'email', 'username'));
			$new_user->type = $request->get('type');
			$new_user->password = bcrypt($request->get('password'));
			if($new_user->save()) {
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"New account has been added.");
				return redirect()->route('system.app_user.index');
			}
			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');

			return redirect()->back();
		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function edit ($id = NULL) {
		$this->data['page_title'] = " :: App User - Edit record";
		$user = User::types(['mentee','mentor'])->where('id',$id)->first();

		if (!$user) {
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Record not found.");
			return redirect()->route('system.app_user.index');
		}

		$this->data['user'] = $user;
		return view('system.app-user.edit',$this->data);
	}

	public function update (AppUserRequest $request, $id = NULL) {
		try {
			$user = User::types(['mentee','mentor'])->where('id',$id)->first();

			if (!$user) {
				session()->flash('notification-status',"failed");
				session()->flash('notification-msg',"Record not found.");
				return redirect()->route('system.app_user.index');
			}

			$user->fill($request->except('password'));
			$user->type = $request->get('type');
			$user->is_approved = $request->get('is_approved');
			if($request->has('password')){
				$user->password = bcrypt($request->get('password'));
			}


			if($user->save()) {
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"Account has been modified successfully.");
				return redirect()->route('system.app_user.index');
			}

			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function import () {
		$this->data['page_title'] = " :: App User - Import accounts";
		return view('system.app-user.import',$this->data);
	}

	public function submit_import(ImportAccountRequest $request){
		Excel::load($request->file('file'), function($reader) {
		    $results = $reader->all();
		    $results->each(function($row) {
		    	$email = $row->email;
		    	$is_exist = User::where('email',$email)
		    					->where('type',$row->type)
		    					->first();
		    	if(!$is_exist){
		    		$new_user = new User;
		    		$new_user->username = $row->contact_number;
		    		$new_user->name = $row->name;
		    		$new_user->email = $row->email;
		    		$new_user->password = bcrypt($row->password);
		    		$new_user->contact_number = $row->contact_number;
		    		$new_user->country_iso = $row->country;
		    		$new_user->country_code = $row->country_code;
		    		$new_user->specialties = $row->specialties;
		    		$new_user->is_approved = "yes";
		    		$new_user->type = $row->type;

		    		if(!in_array($new_user->type, ["mentor","mentee"])){
		    			$new_user->type = "mentee";
		    		}

		    		$new_user->save();
		    	}
		    });
	    	$this->import = true;

		});

		if($this->import === false){
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',"Invalid excel file uploaded.");
			return redirect()->route('system.app_user.index');
		}

		session()->flash('notification-status','success');
		session()->flash('notification-msg',"Mass upload successfully completed.");
		return redirect()->route('system.app_user.index');
	}

	public function destroy ($id = NULL) {
		try {
			$user = User::types(['mentee','mentor'])->where('id',$id)->first();

			if (!$user) {
				session()->flash('notification-status',"failed");
				session()->flash('notification-msg',"Record not found.");
				return redirect()->route('system.app_user.index');
			}

			if($user->delete()) {
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"Record has been deleted.");
				return redirect()->route('system.app_user.index');
			}

			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

}