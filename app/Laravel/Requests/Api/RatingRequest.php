<?php

namespace App\Laravel\Requests\Api;

use App\Laravel\Requests\ApiRequestManager;
// use JWTAuth;

class RatingRequest extends ApiRequestManager
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user = $this->user();

        $rules = [
            'name'   => 'required',
            'rating'  => 'required',
            'type'    => 'required'
        ];

        return $rules;
    }

    public function messages() {

        return [
            'required'  => "This field is required.",
        ];
    }
}
