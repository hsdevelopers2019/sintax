<?php namespace App\Laravel\Requests\System;

use Session,Auth;
use App\Laravel\Requests\RequestManager;

class SpecialtyRequest extends RequestManager{

	public function rules(){

		$id = $this->route('id')?:0;

		$rules = [
			'name'	=> "required|unique:specialty,name,{$id}",
		];

		return $rules;
	}

	public function messages(){
		return [
			'name.unique'	=> "Specialty already used. Please double check your input.",
			'required'	=> "Field is required.",
		];
	}
}