<?php namespace App\Laravel\Requests\System;

use Session,Auth;
use App\Laravel\Requests\RequestManager;

class WaybillReceivingRequest extends RequestManager{

	public function rules(){

		$id = $this->route('id')?:0;

		$rules = [
			'rider_id'	=> "required",
			'remitted_amount'	=> "required|numeric"
		];

		return $rules;
	}

	public function messages(){
		return [
			'rider_id.required'	=> "Please choose a rider before you continue.",
			'remitted_amount.numeric'	=> "Invalid data.",
			'remitted_amount.required'	=> "Please provide the rider remitted amount.",
		];
	}
}