@extends('system._layouts.main')

@section('content')
<div class="main-content container-fluid">
  <div class="row">
    <div class="col-md-6">
      @include('system._components.notifications')
      <div class="panel panel-default panel-border-color panel-border-color-primary">
        <div class="panel-heading panel-heading-divider">Create Record Form<span class="panel-subtitle">Modify employee information.</span></div>
        <div class="panel-body">
          <form method="POST" action="">
            {!!csrf_field()!!}
            <div class="form-group {{$errors->first('type') ? 'has-error' : NULL}}">
              <label>Location Type</label>
              {!!Form::select('type',$types,old('type','metro_manila'),['class' => "form-control"])!!}
              @if($errors->first('type'))
              <span class="help-block">{{$errors->first('type')}}</span>
              @endif
            </div>
            <div class="form-group {{$errors->first('name') ? 'has-error' : NULL}}">
              <label>Location Name</label>
              <input type="text" placeholder="Location name" class="form-control" name="name" value="{{old('name')}}">
              @if($errors->first('name'))
              <span class="help-block">{{$errors->first('name')}}</span>
              @endif
            </div>
            
            <div class="row xs-pt-15">
              <div class="col-xs-6">
                  <button type="submit" class="btn btn-space btn-primary">Create Record</button>
                  <a href="{{route('system.location.index')}}" class="btn btn-space btn-default">Cancel</a>
              </div>
            </div>

          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@stop
@section('page-styles')
<link rel="stylesheet" type="text/css" href="{{asset('assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css')}}"/>
@stop
@section('page-scripts')
<script src="{{asset('assets/lib/moment.js/min/moment.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js')}}" type="text/javascript"></script>
<script type="text/javascript">
  $(function(){
    $('.datepicker').datetimepicker({autoclose: true})
  });
</script>
@stop

