@extends('system._layouts.main')

@section('content')
<div class="main-content container-fluid">
  <div class="row">
    <div class="col-md-12">
      @include('system._components.notifications')
      <div class="panel panel-default panel-table  panel-border-color panel-border-color-success">
        <div class="panel-heading">Record Data </div>
        <div class="panel-body">
          <table class="table table-hover table-wrapper">
            <thead>
              <tr>
                <th >Chat Title</th>
                <th >Creator</th>
                <th style="width:25%;" class="text-left">Last Message Sent</th>
                <th  class="text-center">No. of Participants</th>
                <th  class="text-center">Status</th>
                <th style="width: 10%">Date Created</th>
              </tr>
            </thead>
            <tbody>
              @forelse($chats as $index => $chat)
              <tr>
                <td class="cell-detail"> 
                  <span><a href="{{route('system.chat.show',[$chat->id])}}"><strong>{{$chat->title}}</strong></a></span>
                  <span class="cell-detail-description">{{"ID ".str_pad($chat->id, 4, "0", STR_PAD_LEFT)}}</span>
                </td>
                <td class="cell-detail">
                  <span>{{$chat->author?$chat->author->name:"Unknown User"}}</span>
                </td>
                <td class="cell-detail">
                  <span>{{$chat->new_message}}</span>
                  <span class="cell-detail-description">{{Helper::date_format($chat->new_message_date,"m/d/Y h:i A")}}</span>
                </td>
                <td class="cell-detail text-center">
                  <span>{{Helper::nice_number($chat->participant->count())}}</span>
                </td>
                <td class="cell-detail text-center"> 
                  <span class="text-{{Helper::status_badge($chat->status)}}"><strong>{{Str::title($chat->status)}}</strong></span>
                </td>
                <td class="cell-detail">
                  <span>{{Helper::date_only($chat->created_at)}}</span>
                  <span class="cell-detail-description">{{Helper::date_format($chat->created_at,"h:i A")}}</span>
                </td>
              </tr>
              @empty
              <td colspan="7" class="text-left"><i>No record found yet.</i> </td>
              @endforelse
            </tbody>
          </table>
          
        </div>
        <div class="panel-footer">
          <div class="pagination-wrapper text-center">
            {!!$chats->render()!!}
          </div>
        </div>
      </div>

    </div>
  </div>
</div>
@stop

@section('page-modals')
<div id="confirm-delete" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h1 class="modal-title">Confirm your action</h1>
      </div>

      <div class="modal-body">
        <div role="alert" class="alert alert-warning alert-icon alert-icon-border alert-dismissible">
          <div class="icon"><span class="mdi mdi-close-circle-o"></span></div>
          <div class="message">
            <strong>Warning!</strong> This action can not be undone.
          </div>
        </div>
        <h3 class="text-semibold">Are you sure ...</h3>
        <p>You are about to delete the record?</strong></p>

        <hr>

        <h3 class="text-semibold">What is this message?</h3>
        <p>This dialog appears everytime when the chosen action could hardly affect the system. Usually, it occurs when the system is issued a delete command or upon submission of your task of the day.</p>
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-link" data-dismiss="modal">Cancel</button>
        <a href="#" data-loading-text="<i class='icon-spinner2 spinner position-left'></i> Removing record ..." class="btn btn-primary btn-raised btn-loading" id="btn-confirm-delete">Delete Record</a>
      </div>
    </div>
  </div>
</div>
@stop

@section('page-styles')
<link rel="stylesheet" type="text/css" href="{{asset('assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css')}}"/>
@stop
@section('page-scripts')
<script src="{{asset('assets/lib/moment.js/min/moment.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js')}}" type="text/javascript"></script>
<script type="text/javascript">
  $(function(){
    $('.datepicker').datetimepicker({autoclose: true})
    $(".action-delete").on("click",function(){
      var btn = $(this);
      $("#btn-confirm-delete").attr({"href" : btn.data('url')});
    });

    $('#btn-confirm-delete').on('click', function() {
      $('.btn-link').hide();
          $('.btn-loading').button('loading');
          $('#target').submit();
     });

  });
</script>
@stop

