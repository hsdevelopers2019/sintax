@extends('system._layouts.main')

@section('content')
<div class="main-content container-fluid">
  <div class="row">
    <div class="col-md-12">
      @include('system._components.notifications')
      <div class="panel panel-default panel-table  panel-border-color panel-border-color-success">
        <div class="panel-heading">Record Data
          <div class="tools dropdown">
            <a href="#" type="button" data-toggle="dropdown" class="dropdown-toggle"><span class="icon mdi mdi-more-vert"></span></a>
            <ul role="menu" class="dropdown-menu pull-right">
              <li><a href="{{route('system.app_user.create')}}">Add new account</a></li>
              <li><a href="#">Import User</a></li>
              {{-- <li class="divider"></li> --}}
              {{-- <li><a href="#">Export calendar</a></li> --}}
            </ul>
          </div>
        </div>
        <div class="row table-filters-container">
          <form action="" id="search_filter" >
          <div class="col-md-3">
            <div class="filter-container">
              <label class="control-label">Put any keywords</label>
              {!!Form::hidden('type',$type)!!}
                <input type="text" class="form-control input-sm" name="keyword" value="{{$keyword}}" placeholder="Search keyword eg. johndoe@domain.com, johndoe01">
            </div>
          </div>
          <div class="col-md-6">
            <div class="filter-container">
                <div class="row">
                  <div class="col-xs-6">
                    <label class="control-label">Registered Since:</label>
                    <input type="text" class="form-control input-sm datepick" name="from" data-min-view="2" data-date-format="yyyy-mm-dd"  value="{{$from}}">
                  </div>
                  <div class="col-xs-6">
                    <label class="control-label">To:</label>
                    <input type="text" class="form-control input-sm datepick" name="to" data-min-view="2" data-date-format="yyyy-mm-dd"  value="{{$to}}">
                  </div>
                </div>
            </div>
          </div>
          <div class="col-md-3">
            <div class="row">
              <div class="col-md-12 table-filters"><span class="table-filter-title">&nbsp;</span>
                <div role="group" class="btn-group btn-group-justified">
                  <a href="#" class="btn btn-success btn-export" data-url="{{route('system.app_user.export')}}"><i class="mdi mdi-download"></i> Export </a>
                  <a href="#" class="btn btn-primary btn-search"><i class="mdi mdi-search"></i> Apply Filter</a>
                </div>
              </div>
            </div>
          </div>
          <button type="submit" style="display: none;"></button>
          </form>
        </div>
        <div class="panel-body">
          <table class="table table-hover table-wrapper">
            <thead>
              <tr>
                <th style="width:25%;">Account Name</th>
                <th>Email</th>
                @if(!in_array($current_route,['system.app_user.mentor','system.app_user.mentee']))
                <th class="text-center">Account Type</th>
                @endif
                <th>Status</th>
                <th>Registration Date</th>
                <th class="actions"></th>
              </tr>
            </thead>
            <tbody>
              @forelse($users as $index => $user)
              <tr>

                <td class="user-avatar"> 
                  <img src="{{$user->avatar}}" alt="{{$user->name}}">
                  <a href="#"><strong>{{$user->name}}</strong></a>
                </td>
                <td class="cell-detail"> 
                  <span><a href="mailto:{{$user->email}}">{{$user->email}}</a></span>
                  <span class="cell-detail-description">{{"@".$user->username}}</span>
                  <span>{{str_pad($user->id,4,"0",STR_PAD_LEFT)}}</span>
                  <span class="cell-detail-description">Account ID</span>
                </td>
                @if(!in_array($current_route,['system.app_user.mentor','system.app_user.mentee']))
                <td class="text-center"> <span class="label label-{{Helper::account_type($user->type)}}">{{Str::title($user->type)}}</span></td>
                @endif
                <td class="cell-detail">
                  <span class="text-{{$user->is_approved == "yes" ? "success" : ($user->is_approved == "pending" ? "warning" : "danger")}}"><strong>{{$user->is_approved == "yes" ? "Approved" : ($user->is_approved == "pending" ? "Pending for Approval": "Declined")}}</strong></span>
                  <span class="cell-detail-description">Account Approval</span>

                  <span class="text-{{$user->is_verified == "yes" ? "success" : "warning"}}"><strong>{{$user->is_verified == "yes" ? "Verified Email" : "Pending"}}</strong></span>
                  <span class="cell-detail-description">Email Verification</span>
                </td>
                <td class="cell-detail">
                  <span>{{Helper::date_only($user->created_at)}}</span>
                  <span class="cell-detail-description">{{Helper::date_format($user->created_at,"h:i A")}}</span>
                  <span>{!!$user->last_activity  ? Helper::date_format($user->last_activity,"m/d/Y h:i A") : "<i>n/a</i>"!!}</span>
                  <span class="cell-detail-description">Last Active</span>
                </td>
                <td class="actions">
                    <div class="btn-group btn-hspace">
                      <button type="button" data-toggle="dropdown" class="btn btn-default dropdown-toggle" aria-expanded="false"><i class="icon icon-left mdi mdi-settings"></i> Actions <span class="icon-dropdown mdi mdi-chevron-down"></span></button>
                      <ul role="menu" class="dropdown-menu">
                        <li><a href="{{route('system.app_user.edit',[$user->id])}}">Edit Record</a></li>
                        <li class="divider"></li>
                        <li><a class="action-delete" href="#" data-url="{{route('system.app_user.destroy',[$user->id])}}" data-toggle="modal" data-target="#confirm-delete" title="Remove Record">Remove Record</a></li>
                      </ul>
                    </div>
                </td>
              </tr>
              @empty
              <td colspan="5" class="text-center"><i>No record found yet.</i> <a href="{{route('system.app_user.create')}}"><strong>Click here</strong></a> to create one.</td>
              @endforelse
            </tbody>
          </table>
          
        </div>
        <div class="panel-footer">
          <div class="pagination-wrapper text-center">
            {!!$users->render()!!}
          </div>
        </div>
      </div>

    </div>
  </div>
</div>
@stop

@section('page-modals')
<div id="confirm-delete" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h1 class="modal-title">Confirm your action</h1>
      </div>

      <div class="modal-body">
        <div role="alert" class="alert alert-warning alert-icon alert-icon-border alert-dismissible">
          <div class="icon"><span class="mdi mdi-close-circle-o"></span></div>
          <div class="message">
            <strong>Warning!</strong> This action can not be undone.
          </div>
        </div>
        <h3 class="text-semibold">Are you sure ...</h3>
        <p>You are about to submit the task you've done and logging out, would you like to proceed? <strong>Or you just want to save it as draft?</strong></p>

        <hr>

        <h3 class="text-semibold">What is this message?</h3>
        <p>This dialog appears everytime when the chosen action could hardly affect the system. Usually, it occurs when the system is issued a delete command or upon submission of your task of the day.</p>
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-link" data-dismiss="modal">Cancel</button>
        <a href="#" data-loading-text="<i class='icon-spinner2 spinner position-left'></i> Removing record ..." class="btn btn-primary btn-raised btn-loading" id="btn-confirm-delete">Delete Record</a>
      </div>
    </div>
  </div>
</div>
@stop

@section('page-styles')
<link rel="stylesheet" type="text/css" href="{{asset('assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css')}}"/>
@stop
@section('page-scripts')
<script src="{{asset('assets/lib/moment.js/min/moment.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js')}}" type="text/javascript"></script>
<script type="text/javascript">
  $(function(){
    $(".btn-search").on("click",function(e){
      $("#search_filter").submit();
    });

    $(".btn-export").on("click",function(e){
      $("#search_filter").attr("action",$(this).data("url"))
      $("#search_filter").submit();
    });
    
    $(".action-delete").on("click",function(){
      var btn = $(this);
      $("#btn-confirm-delete").attr({"href" : btn.data('url')});
    });

    $('#btn-confirm-delete').on('click', function() {
      $('.btn-link').hide();
          $('.btn-loading').button('loading');
          $('#target').submit();
     });

    $('input.datepick').on('focus',function(){
        $(this).datetimepicker({autoclose: true});
        $(this).datetimepicker('show');
        $(this).on('changeDate', function(ev){
            // do stuff
        }).on('hide', function(){
                $(this).datetimepicker('remove');
            });
    });

    // $('.table-wrapper').find(".btn-group").children("button").addClass("btn-xs");

  });
</script>
@stop

