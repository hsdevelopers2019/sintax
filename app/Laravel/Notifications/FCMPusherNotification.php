<?php

namespace App\Laravel\Notifications;

use App\Laravel\Models\ChatConversation;
use App\Laravel\Models\MentorshipConversation;


use Helper;
use Illuminate\Bus\Queueable;
use NotificationChannels\FCM\FCMMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use App\Laravel\Notifications\Channels\BroadcastChannel;
use App\Laravel\Notifications\Messages\BroadcastGroupChatMessage;
use App\Laravel\Notifications\Messages\BroadcastMessage;


class FCMPusherNotification extends Notification implements ShouldQueue
{
     use Queueable;

    /**
     * The notification data.
     *
     */
    protected $data,$type,$message_id;

    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 3;

    public function __construct(array $data,$message_id = "0",$type = "")
    {
        $this->data = collect($data);
        $this->type = $type;
        $this->message_id = $message_id;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [ 'fcm',BroadcastChannel::class ];
    }

    /**
     * Get the fcm representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \NotificationChannels\FCM\FCMMessage
     */
    public function toFCM($notifiable)
    {
        $notification = [
            'title' => $this->data->get('title'),
            'body' => $this->data->get('content'),
        ];

        $data = [
            'title' => $this->data->get('title'),
            'body' => $this->data->get('content'),
            'type' => $this->data->get('type'),
            'display_type'  => "", //for future notification with big image when needed
            // 'counter_badge' => 'users.'.$this->id,
            'counter_badge' => 0,
            'reference_id' => $this->data->get('reference_id'),
            'thumbnail' => $this->data->get('thumbnail'),
            'event' => get_class($this),
        ];

        return (new FCMMessage())
            ->notification($notification)
            ->data($data);
    }

    public function toBroadcast($notifiable)
    {
        $unread = $notifiable->unreadNotifications()
                    ->where(function($query) use($notifiable) {
                        if($notifiable->last_notification_check) {
                            $date = Helper::datetime_db($notifiable->last_notification_check);
                            $query->where('created_at', '>=', $date);
                        }
                    })
                    ->count();
        /*switch($this->type){
            case "GROUPCHAT":
                $message = ChatConversation::find($this->message_id);
                // return new BroadcastGroupChatMessage($message);
                // return new BroadcastMessage(['message' => $message,'channel_name' => "groupchat:{$message->chat_id}","event_name" => "new_message"]);

            break;

            case "MENTORSHIP":
                $message = MentorshipConversation::find($this->message_id);
                // return new BroadcastGroupChatMessage($message);
                // return new BroadcastMessage(['message' => $message,'channel_name' => "mentorship:{$message->mentorship_id}","event_name" => "new_message"]);

            break;
        }  */          
    }
}
