<?php 

namespace App\Laravel\Transformers;

use Input,Str;
use JWTAuth, Carbon, Helper;
use App\Laravel\Models\Qr;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use League\Fractal\TransformerAbstract;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Laravel\Transformers\MasterTransformer;

class QrTransformer extends TransformerAbstract{

	protected $availableIncludes = [
    ];


	public function transform(Qr $qr) {

	    return [
	     	'id' => $qr->id ?:0,
	     	'brand' => $qr->brand,
	     	'description' => $qr->description,
	     	'code' => $qr->qr_code
	     	
	     ];
	}
}