<?php

namespace App\Laravel\Models;

use Illuminate\Database\Eloquent\Model;
use App\Laravel\Traits\DateFormatterTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class MentorshipConversation extends Model
{
    use DateFormatterTrait,SoftDeletes;

	protected $table = "mentorship_conversation";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'content'
    ];

    protected $appends = ['new_directory'];

    public $timestamps = true;

    public function getNewDirectoryAttribute(){
        return str_replace(env("BLOB_STORAGE_URL"), env("CDN_STORAGE_URL"), $this->directory);
    }


    public function sender(){
        return $this->belongsTo("App\Laravel\Models\User", "sender_user_id", "id");
    }

    public function mentorship(){
        return $this->belongsTo("App\Laravel\Models\Mentorship", "mentorship_id", "id");
    }

    public function participant(){
        return $this->belongsTo("App\Laravel\Models\MentorshipParticipant", "sender_user_id", "user_id")->where('mentorship_id',$this->mentorship_id);
    }
}
