<?php

namespace App\laravel\models;

use Illuminate\Database\Eloquent\Model;

class Qr extends Model
{
      protected $fillable = [
        'brand', 'description', 'qr_code',
    ];
}
