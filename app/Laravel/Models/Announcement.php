<?php

namespace App\Laravel\Models;

use Illuminate\Database\Eloquent\Model;
use App\Laravel\Traits\DateFormatterTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use Auth,Helper;
class Announcement extends Model
{
    use DateFormatterTrait,SoftDeletes;

	protected $table = "announcement";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title','content','user_notified'];

    protected $appends = ['thumbnail','excerpt','new_directory'];

    public $timestamps = true;

    public function getNewDirectoryAttribute(){
        return str_replace(env("BLOB_STORAGE_URL"), env("CDN_STORAGE_URL"), $this->directory);
    }

    public function author(){
        return $this->hasOne('App\Laravel\Models\User','id','user_id');
    }

    public function getExcerptAttribute(){
        return Helper::get_excerpt($this->content);
    }

    public function getThumbnailAttribute(){
        if($this->filename){
            return "{$this->new_directory}/resized/{$this->filename}";
        }

        return asset('placeholder/user.jpg');
    }

    public function scopeKeyword($query, $keyword = NULL){
        if($keyword){
            $keyword = strtolower($keyword);
            return $query->whereRaw("LOWER(title) LIKE '{$keyword}%'")
                        ->orWhereRaw("LOWER(content) LIKE '{$keyword}%'");
        }
    }
}
