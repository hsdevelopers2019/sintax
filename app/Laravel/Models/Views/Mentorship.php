<?php

namespace App\Laravel\Models\Views;

use Illuminate\Database\Eloquent\Model;
use App\Laravel\Traits\DateFormatterTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class Mentorship extends Model
{
    use DateFormatterTrait,SoftDeletes;

	protected $table = "view_mentorship_header";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    protected $appends = ['new_directory','new_message','new_message_date'];

    public $timestamps = true;

    public function getNewDirectoryAttribute(){
        return str_replace(env("BLOB_STORAGE_URL"), env("CDN_STORAGE_URL"), $this->directory);
    }

    public function getNewMessageAttribute(){
        $latest_message = $this->latest_message()->first();
        return $latest_message?$latest_message->content:"n/a";
    }

    public function getNewMessageDateAttribute(){
        $latest_message = $this->latest_message()->first();
        return $latest_message?$latest_message->created_at : $this->created_at;
    }

    public function author(){
        return $this->hasOne('App\Laravel\Models\User','id','owner_user_id');
    }

    public function conversation(){
        return $this->hasMany('App\Laravel\Models\MentorshipConversation','mentorship_id','id');
    }

    public function latest_message(){
        return $this->hasOne('App\Laravel\Models\MentorshipConversation','mentorship_id','id')->orderBy('created_at','DESC');
    }

    public function participant(){
        return $this->hasMany('App\Laravel\Models\MentorshipParticipant','mentorship_id','id');
    }

    public function mentor(){
        return $this->belongsTo('App\Laravel\Models\User','mentor_user_id','id');
    }

    public function mentee(){
        return $this->belongsTo('App\Laravel\Models\User','mentee_user_id','id');
    }

    public function scopeKeyword($query, $keyword = NULL){
        if($keyword){
            $keyword = strtolower($keyword);
            return $query->whereRaw("LOWER(title) LIKE '{$keyword}%'");
        }
    }
}
