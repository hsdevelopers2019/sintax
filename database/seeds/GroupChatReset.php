<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Laravel\Models\Chat;
use App\Laravel\Models\ChatParticipant;
use App\Laravel\Models\ChatConversation;

class GroupChatReset extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Chat::truncate();
        ChatParticipant::truncate();
        ChatConversation::truncate();
    }
}
